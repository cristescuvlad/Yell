package com.example.vlad.yell;

/**
 * Created by Vlad on 30-Jun-18.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import org.dyn4j.dynamics.Body;
import org.dyn4j.dynamics.BodyFixture;
import org.dyn4j.dynamics.World;
import org.dyn4j.geometry.Circle;
import org.dyn4j.geometry.MassType;
import org.dyn4j.geometry.Rectangle;
import org.dyn4j.geometry.Vector2;

import java.util.Random;
import java.util.concurrent.locks.ReentrantLock;

public class MainView extends SurfaceView implements Runnable {

    public MainView(Context context) {
        super(context);

        ourHolder = getHolder();
        paint = new Paint();
        playing = true;
        rnd = new Random();
        mLock = new ReentrantLock();
        lastUpdate = System.currentTimeMillis();

        initWorld();
    }

    @Override
    public void run() {
        while (playing) {
            update();
            draw();
        }
    }

    public void update() {

        millisSinceEpoch = System.currentTimeMillis();

        lastUpdate = System.currentTimeMillis() - lastUpdate;
        mWorld.update(lastUpdate/1000);
        Log.e("VLADC","millis: " + (System.currentTimeMillis() - millisSinceEpoch));

        frames++;


        float perturbance = 0;
        mLock.lock();  // block until condition holds
        try {
            perturbance = mPerturbance;
        }
        finally {
            mLock.unlock();
            Vector2 f = new Vector2(0,-1);
            double d = f.normalize() * 10;
            f.multiply(4000*perturbance / d);
            mBody.applyForce(f);
        }

        //https://stackoverflow.com/questions/4513902/android-bouncing-ball
    }

    public void draw() {
        if (ourHolder.getSurface().isValid()) {
            canvas = ourHolder.lockCanvas();

            paint.setColor(Color.BLUE);
            paint.setStyle(Paint.Style.FILL);
            canvas.drawPaint(paint);

            paint.setColor(Color.rgb(255,69,0));
            paint.setTextSize(48f);

            mLock.lock();  // block until condition holds
            try {
                if(dbgText != null){
                    canvas.drawText(dbgText,100,100, paint);
                }
            }
            finally {
                mLock.unlock();
            }

            Vector2 pos = mBody.getWorldCenter();
            canvas.drawCircle((float)pos.x, (float)pos.y, (float)mBody.getRotationDiscRadius(), paint);

            pos = mFloor.getWorldCenter();
            float l = (float)pos.x-26;
            float t = (float)(pos.y-2);
            float r = (float)pos.x+26;
            float b = (float)(pos.y+2);
            canvas.drawRect(l,t,r,b, paint); //w16,h01


            pos = mCeiling.getWorldCenter();

            l = (float)pos.x-26;
            t = (float)(pos.y-2);
            r = (float)pos.x+26;
            b = (float)(pos.y+2);

            canvas.drawRect(l,t,r,b, paint); //w16,h01

            ourHolder.unlockCanvasAndPost(canvas);
        }
    }

    public void pause() {
        playing = false;
        try {
            gameThread.join();
        } catch (InterruptedException e) {
            Log.e("Error:", "joining thread");
        }
    }

    public void resume() {
        playing = true;
        gameThread = new Thread(this);
        gameThread.start();
    }

    public void stop() {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        int x = (int) event.getX();
        int y = (int) event.getY();

        if(x < 5 || y < 5 || x > this.getWidth()-100 || y > this.getHeight()-100){
            return false;
        }

        return false;
    }

    private void initWorld()
    {
        mWorld = new World();
        mWorld.setGravity(new Vector2(0.0, 10 * 9.8));

        BodyFixture fixture = new BodyFixture(new Circle(50));
        fixture.setDensity(BodyFixture.DEFAULT_DENSITY);
        fixture.setFriction(BodyFixture.DEFAULT_FRICTION);
        fixture.setRestitution(BodyFixture.DEFAULT_RESTITUTION);

        mBody = new Body();
        mBody.addFixture(fixture);
        mBody.translate(200.0, 190.0);
        mBody.setMass(MassType.NORMAL);

        mWorld.addBody(mBody);


        Rectangle floorRect = new Rectangle(16.0, 0.1);
        mFloor = new Body();
        mFloor.addFixture(floorRect);
        mFloor.setMass(MassType.INFINITE);
        // move the floor down a bit
        mFloor.translate(200.0, 1000.0);

        mWorld.addBody(mFloor);

        Rectangle topRect = new Rectangle(16.0, 0.1);
        mCeiling = new Body();

        mCeiling.addFixture(topRect);
        mCeiling.setMass(MassType.INFINITE);

        // move the ceiling up
        mCeiling.translate(200.0, 100.0);

        mWorld.addBody(mCeiling);
    }

    public void setDebugText(String text){
        mLock.lock();  // block until condition holds
        try {
            dbgText = text;
        }
        finally {
            mLock.unlock();
        }
    }

    public void giveCirclePerturbance(float perturbance){

        mLock.lock();  // block until condition holds
        try {
            mPerturbance = perturbance;
        }
        finally {
            mLock.unlock();
        }
    }

    //member vars

    private long millisSinceEpoch = 0;
    private long lastUpdate = 0;
    private Thread gameThread = null;
    private ReentrantLock mLock = null;
    private SurfaceHolder ourHolder = null;
    private volatile boolean playing = false;
    private Canvas canvas = null;
    private Paint paint = null;
    private Random rnd = null;
    private String dbgText = null;
    private float mPerturbance = 0;

    World mWorld = null;
    Body mBody = null;
    Body mFloor = null;
    Body mCeiling = null;

    private int frames = 0;

}