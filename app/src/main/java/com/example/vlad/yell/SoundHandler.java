package com.example.vlad.yell;

import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;

import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by Vlad on 04-Jul-18.
 */

//https://stackoverflow.com/questions/8499042/android-audiorecord-example#

public class SoundHandler implements Runnable {

    SoundHandler() {
        mLock = new ReentrantLock();
    }

    @Override
    public void run() {
        // Moves the current Thread into the background

        android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

        startRecording(); //records on a different thread

        while(true){
            update();
        }
    }

    public void update() {
        retrieveAudio();
    }

    private void retrieveAudio(){

        short[] data = new short[mBufferElements2Rec];

        // gets the voice output from microphone to byte format
        int readSize = recorder.read(data, 0, mBufferElements2Rec);

        // Write the output audio in byte
        mLock.lock();  // block until condition holds
        try {
            short[] currentBuff = mData;

            int currentBuffSize = (mData != null ? mData.length : 0);

            mData = new short[currentBuffSize + readSize];
            if(currentBuff != null){
                System.arraycopy(currentBuff,0,mData, 0, currentBuffSize);
            }
            if(data != null){
                System.arraycopy(data,0, mData, currentBuffSize, readSize);
            }

            //dbgWriteToFile(extractAudioData());
        }
        finally {
            mLock.unlock();
        }
    }

    private void startRecording(){

        recorder = new AudioRecord(MediaRecorder.AudioSource.MIC,
                RECORDER_SAMPLERATE, RECORDER_CHANNELS,
                RECORDER_AUDIO_ENCODING, mBufferElements2Rec * mBytesPerElement);

        recorder.startRecording();
    }

    private void stopRecording() {
        // stops the recording activity
        if (null != recorder) {
            recorder.stop();
            recorder.release();
            recorder = null;
        }
    }

    public short[] extractAudioData(){

        short data[] = null;

        mLock.lock();  // block until condition holds
        try {
            data = mData;
            mData = null;
        }
        finally {
            mLock.unlock();
        }
        return data;
    }


    private static final int RECORDER_SAMPLERATE = 44100;
    private static final int RECORDER_CHANNELS = AudioFormat.CHANNEL_IN_MONO;
    private static final int RECORDER_AUDIO_ENCODING = AudioFormat.ENCODING_PCM_16BIT;

    private int mBufferElements2Rec = 1024; // want to play 2048 (2K) since 2 bytes we use only 1024
    private int mBytesPerElement = 2; // 2 bytes in 16bit format

    private short[] mData = null;

    private AudioRecord recorder = null;

    private ReentrantLock mLock = null;

}
