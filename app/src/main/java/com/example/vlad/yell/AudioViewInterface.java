package com.example.vlad.yell;

/**
 * Created by Vlad on 02-Sep-18.
 */

public class AudioViewInterface implements Runnable {

    public AudioViewInterface(SoundHandler soundHandler, MainView view){
        mSoundHandler = soundHandler;
        mView = view;
    }

    @Override
    public void run() {

        while(true){
            update();
        }

    }

    public void update() {

        if(mSoundHandler != null){
            short[] audioData = mSoundHandler.extractAudioData();

            if(audioData != null){
                //TODO: do stuff with audio data ! :)
                //Utils.dbgWriteToFile(audioData);
                float absMean = (float)Utils.absMean(audioData);

                mView.setDebugText(Double.toString(absMean));
                mView.giveCirclePerturbance(absMean);
            }
        }
    }

    private SoundHandler mSoundHandler = null;
    private MainView mView = null;

}
