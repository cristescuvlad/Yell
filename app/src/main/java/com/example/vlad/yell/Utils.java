package com.example.vlad.yell;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Created by Vlad on 02-Sep-18.
 */

public class Utils {


    public static void dbgWriteToFile(short data[]){

        FileOutputStream os = null;

        try {
            os = new FileOutputStream("/sdcard/voice16bitmono.pcm", true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        try {
            // writes the data to file from buffer
            // stores the voice buffer
            byte bData[] = short2byte(data);
            os.write(bData, 0, data.length * 2); //short size = 2
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            os.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];
        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;
    }

    public static double mean(short[] m) {
        double sum = 0;
        for (int i = 0; i < m.length; i++) {
            sum += m[i];
        }
        return sum / m.length;
    }

    public static double absMean(short[] m) {
        double sum = 0;
        for (int i = 0; i < m.length; i++) {
            sum += Math.abs(m[i]);
        }
        return sum / m.length;
    }
}
