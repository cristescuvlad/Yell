package com.example.vlad.yell;

import android.Manifest;
import android.content.pm.ActivityInfo;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.content.pm.PackageManager;

public class MainActivity extends AppCompatActivity {

    //https://developer.android.com/guide/topics/media/mediarecorder

    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private boolean permissionToRecordAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO};

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        // Record to the external cache directory for visibility

        ActivityCompat.requestPermissions(this, permissions, REQUEST_RECORD_AUDIO_PERMISSION);

        this.setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        super.onCreate(savedInstanceState);

        mGameView = new MainView(this);
        setContentView(mGameView);

        mSoundHandler = new SoundHandler();
        Thread audioThread = new Thread(mSoundHandler);
        audioThread.start();

        mAudioViewInterface = new AudioViewInterface(mSoundHandler, mGameView);
        Thread interfaceThread = new Thread(mAudioViewInterface);
        interfaceThread.start();
    }

    //TODO: implement pause and resume for audiohandler and audioview interface

    // This method executes when the player quits the game
    @Override
    protected void onPause() {
        super.onPause();
        mGameView.pause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mGameView.resume();
    }

    @Override
    public void onStop() {
        super.onStop();
        mGameView.stop();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionToRecordAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
        }
        if (!permissionToRecordAccepted ) finish();

    }

    private MainView mGameView = null;
    private SoundHandler mSoundHandler = null;
    private AudioViewInterface mAudioViewInterface = null;
}
